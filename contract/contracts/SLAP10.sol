pragma solidity ^0.7.0;
pragma experimental ABIEncoderV2;

contract SLAP10 {
	
	struct Bounty {
		address sender;
		uint id;

		//string name;
		//string description;
		uint budget;
		//string keyword1;
		//string keyword2;
		//string keyword3;
		uint startDate;
		uint duration;

		bool isDone;
		//string tos;
		//string url;

		uint totalHunters;
		
		//id of bounty hunters
		uint[] hunters;
		string json;

		// number of users, rank1, rank2, eth amount
		uint[] conds;
	}

	// bountyID => (indx => BountyHunter ID)
	//mapping(uint => mapping(uint => uint)) bountyHuntersApplied;

	struct User {
		uint id;
		uint bountyCount;
		uint huntersCount;
		mapping(uint => uint) bounties;
		mapping(uint => uint) hunters;
	}

	struct BountyHunter {
		uint id;
		uint bountyId;
		uint userId;
		string url;
	}

	mapping(address => User) public users;
	mapping(uint => Bounty) public bounties;
	mapping(uint => BountyHunter) public bountyHunters;

	mapping(uint => mapping(uint => bool) ) public bountyUsers;
	
	uint minBudget = 0.1 ether;

	address admin = msg.sender;

	mapping(uint => address) public idToAddress;
	uint public lastUserId = 1;
	uint public lastBountyId = 1;
	uint public lastBountyHunterId = 1;


	event NewBounty(
		address indexed sender,
		uint indexed id,
		//string name,
		//string description,
		uint budget,
		//string keyword1,
		//string keyword2,
		//string keyword3,
		uint startDate,
		uint duration,

		//string tos,
		//string url
		string json
	);

	modifier isAdmin(){
		require(msg.sender == admin, "AdminOnly function");
		_;
	}

	modifier checkMin(){
		require(msg.value >= minBudget, "Budget is too low");
		_;
	}
	
	constructor(){

	}
	
	function addBounty(
		string memory _json,
		//string memory _description,
		//string memory _keyword1,
		//string memory _keyword2,
		//string memory _keyword3,
		uint _startDate,
		uint _duration,
		uint[] memory _conds
		//string memory _tos,
		//string memory _url
		) checkMin public payable {

		if(users[msg.sender].id <= 0){
			_registerUser(msg.sender);
		}

		Bounty storage bounty = bounties[lastBountyId++];

		bounty.id = lastBountyId-1;

		//bounty.name = _name;
		bounty.sender = msg.sender;
		//bounty.description = _description;
		bounty.budget = msg.value;
		//bounty.keyword1 = _keyword1;
		//bounty.keyword2 = _keyword2;
		//bounty.keyword3 = _keyword3;
		bounty.startDate = _startDate;
		bounty.duration = _duration;
		bounty.conds = _conds;
		bounty.isDone = false;

		bounty.json = _json;

		//bounty.tos = _tos;
		//bounty.url = _url;

		users[msg.sender].bounties[
			users[msg.sender].bountyCount++
		] = bounty.id;

		emit NewBounty(
			msg.sender,
			lastBountyId-1,
			//_name,
			//_description,
			msg.value,
			//_keyword1,
			//_keyword2,
			//_keyword3,
			_startDate,
			_duration,
			//_tos,
			//_url
			_json
		);
	}

	function addHunter(uint _bountyId, string memory _url) public{
		if(users[msg.sender].id <= 0){
			_registerUser(msg.sender);
		}
		require(_bountyId < lastBountyId && _bountyId > 0, "Invalid Bounty");

		require(!bountyUsers[_bountyId][users[msg.sender].id], "Already applied");
		bountyUsers[_bountyId][users[msg.sender].id] = true;

		BountyHunter storage bh = bountyHunters[lastBountyHunterId++];

		bh.id = lastBountyHunterId-1;
		bh.bountyId = _bountyId;
		bh.userId = users[msg.sender].id;
		bh.url = _url;

		users[msg.sender].hunters[users[msg.sender].huntersCount++] = bh.id;

		bounties[_bountyId].hunters.push(bh.id);
		bounties[_bountyId].totalHunters++;
	}

	function _registerUser(address _addr) private{
		User storage user = users[_addr];
		user.id = lastUserId;
		idToAddress[lastUserId] = _addr;
		lastUserId++;
	}

	function viewInfo(uint index) public view returns (
		Bounty memory bounty
	)
	{
		bounty = bounties[index];
	}

	function getBounties(uint[] memory indexes) public view returns(
		Bounty[] memory,
		uint[][] memory
	){
		Bounty[] memory _bounties = new Bounty[](indexes.length);
		uint[][] memory _conds = new uint[][](indexes.length);

		for(uint i = 0;  i < indexes.length; i++){
			_bounties[i] = bounties[indexes[i]];
			_conds[i] = bounties[indexes[i]].conds;
		}
		return (_bounties, _conds);
	}

	function getHunters(uint[] memory indexes) public view returns(
		BountyHunter[] memory
	){
		BountyHunter[] memory _bhs = new BountyHunter[](indexes.length);

		for(uint i = 0;  i < indexes.length; i++){
			_bhs[i] = bountyHunters[indexes[i]];
		}
		return _bhs;
	}

	function userBounties(address user, uint[] memory indexes) public view returns(
		Bounty[] memory,
		uint[][] memory
	){
		Bounty[] memory _bounties = new Bounty[](indexes.length);
		uint[][] memory _conds = new uint[][](indexes.length);

		for(uint i = 0;  i < indexes.length; i++){
			_bounties[i] = bounties[users[user].bounties[indexes[i]]];
			_conds[i] = bounties[users[user].bounties[indexes[i]]].conds;
		}
		return (_bounties, _conds);
	}

	function userHunters(address user, uint[] memory indexes) public view returns(
		BountyHunter[] memory
	){
		BountyHunter[] memory _hunters = new BountyHunter[](indexes.length);

		for(uint i = 0;  i < indexes.length; i++){
			_hunters[i] = bountyHunters[users[user].hunters[indexes[i]]];
		}
		return _hunters;
	}

	function getBountyHunters(uint _bountyId) public view returns(
		BountyHunter[] memory
	){
		BountyHunter[] memory _hunters = new BountyHunter[](bounties[_bountyId].hunters.length);
		for(uint i = 0;  i < bounties[_bountyId].hunters.length; i++){
			_hunters[i] = bountyHunters[bounties[_bountyId].hunters[i]];
		}
		return _hunters;
	}

	function updateAdmin(address _admin) external isAdmin returns (bool) {
		require(_admin != address(0));
		admin = _admin;		
		return true;
	}
}
