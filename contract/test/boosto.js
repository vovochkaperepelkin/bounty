const SLAP10 = artifacts.require('./SLAP10.sol');

const toWei = (number) => number * Math.pow(10, 18);
const fromWei = (x) => x/1e18;
const addr0 = "0x0000000000000000000000000000000000000000";

const transaction = (address, wei) => ({
    from: address,
    value: wei
});

const fail = (msg) => (error) => assert(false, error ?
    `${msg}, but got error: ${error.message}` : msg);

const revertExpectedError = async(promise) => {
    try {
        await promise;
        fail('expected to fail')();
    } catch (error) {
        assert(error.message.indexOf('revert') >= 0 || error.message.indexOf('invalid opcode') >= 0,
            `Expected revert, but got: ${error.message}`);
    }
}

const timeController = (() => {

    const addSeconds = (seconds) => new Promise((resolve, reject) =>
        web3.currentProvider.sendAsync({
            jsonrpc: "2.0",
            method: "evm_increaseTime",
            params: [seconds],
            id: new Date().getTime()
        }, (error, result) => error ? reject(error) : resolve(result.result)));

    const addDays = (days) => addSeconds(days * 24 * 60 * 60);
    const addHours = (hours) => addSeconds(hours * 60 * 60);

    const currentTimestamp = () => web3.eth.getBlock(web3.eth.blockNumber).timestamp;

    return {
        addSeconds,
        addDays,
        addHours,
        currentTimestamp
    };
})();

const ethBalance = (address) => web3.eth.getBalance(address);

contract('SLAP10', accounts => {

    const admin = accounts[0];
    const oneEth = toWei(1);

    const testAsync = async() => {
        const response = await new Promise(resolve => {
            setTimeout(() => {
                //resolve("async await test...");
            }, 1000);
        });
        //console.log(response);
    }

    const createToken = () => SLAP10.new({ from: admin });

    it('jiashunran test', async() => {
        const cp = await createToken();
        await cp.addBounty(
            "test", "test", "test", "test", "test",
            123456,
            123456,
            'aa',
            'bb',
            {
                from: admin,
                value: toWei(1)
            }
        );
        await cp.addBounty(
            "test", "test", "test", "test", "test",
            123456,
            123456,
            'aa',
            'bb',
            {
                from: admin,
                value: toWei(1)
            }
        );
        const a = await cp.userBounties(admin, [0,1]);
        console.log(a);
    });

});