const ropstenEtherScanUrl = 'https://ropsten.etherscan.io/tx/';
const bountyContractAddress = '0xe9B622a58D56E91f74bE73798A0F9Ed72832379a';
const appVersion = 1;
var bountyContract = null;
var balance = null;
var currentAccount = null;

var checkMetamask = new Promise((resolve, reject) => {
  if (typeof window.ethereum !== 'undefined') {
    ethereum._metamask.isUnlocked().then(res => {
      if (!res) {
        window.location = window.location.origin;
        reject(false);
      } else {
        web3.eth.getAccounts().then(accounts => {
          if (accounts.length == 0) window.location = window.location.origin; 
          currentAccount = accounts[0];
          resolve(true)
        })
      }
    })
    .catch(err => {
      console.log(err, 'err')
      reject(err);
    })

  } else {
    window.location = window.location.origin;
  }
});


var getBalance = function () {
  web3.eth.getBalance(currentAccount, function(error, result) {
    if (result) {
      balance = web3.utils.fromWei(result, 'ether');
      $('#balanceId').text(Number(balance).toFixed(4));
    } else {
      console.log(error);
    }
  });
};

var getParam = function (name) {
  return (location.search.split(name + '=')[1] || '').split('&')[0];
};

var truncateAddress = function (address) {
  return address.slice(1, 7) + '...' + address.slice(address.length - 6);
};

var isNumeric = function (value) {
  return /^\d+$/.test(value);
};

var createIdsList = function (start, pageLength, count) {
  let ids = [];
  let pageNumber = (start / pageLength) + 1;
  let end = (count - 1) - ((pageNumber - 1) * pageLength);
  for(let i=0; i < pageLength; i++){
    let id = (end - i);
    if (id < 0) {
      break;
    }
    ids.push(id);
  }
  return ids;
};

var mergeBountyHunters = function (bounties, hunters) {
  return bounties.map((bounty, index)=> {
    let json = JSON.parse(bounty.json);
    return {
      name: json.name,
      keyword1: json.kwds[0],
      keyword2: json.kwds[1],
      keyword3: json.kwds[2],
      budget: web3.utils.fromWei(bounty.budget, 'ether'),
      url: hunters[index].url,
      bountyId: bounty.id
    }
  })
};


toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "10000",
  "extendedTimeOut": "10000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

$(function () {
  bountyContract = new web3.eth.Contract(contractAbi, bountyContractAddress);
});
