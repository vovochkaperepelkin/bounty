var bountyCount = null;
var hunterCount = null;
var myBountiesTable = null;
var myHuntersTable = null;

var onChangeAccount = function () {
  ethereum.on('accountsChanged', (accounts) => {
    if (accounts.length === 0) {
      window.location = window.location.origin;
    } else {
      currentAccount = web3.utils.toChecksumAddress(accounts[0]);
      $('#accountAddressMyBounties').text(truncateAddress(currentAccount));
      getUsersCount();
    }
  });
};

var drawMyBountiesTable = function () {
  if (myBountiesTable) {
    myBountiesTable.destroy();
  }
  myBountiesTable = $('#myBountiesTable').DataTable({
    "processing": true,
    "serverSide": true,
    'ajax': function (data, callback, settings) {
      let bounties = [];
      let ids = [];
      let ajaxSrc = null;
      let recordsTotal = bountyCount;
      if (!web3.utils.isAddress(data.search.value) &&
          data.search.value !== '' &&
          !isNumeric(data.search.value)) {
        return callback({
          'data': bounties,
          'draw': data.draw,
          'recordsTotal': 0,
          'recordsFiltered': 0
        })
      } else if (isNumeric(data.search.value)) {
          ids.push(data.search.value);
          let bountyId = data.search.value;
          bountyContract.methods.viewInfo(bountyId).call({
            from: currentAccount
          })
          .then(res => {
            if (res.sender === currentAccount) {
              const json = JSON.parse(res.json);
              bounties.push({
                id: res.id,
                name: json.name,
                budget: web3.utils.fromWei(res.budget, 'ether'),
                startDate: moment.unix(Number(res.startDate)).format("YYYY-MM-DD HH:mm:ss"),
                keywords: `${json.kwds[0]}, ${json.kwds[1]}, ${json.kwds[2]}`,
                hunters: res.hunters.length,
              })

            } else {
              recordsTotal = 0;
              recordsFiltered = 0;
            }
            ajaxSrc = {
              'data': bounties,
              'draw': data.draw,
              'recordsTotal': bounties.length,
              'recordsFiltered': bounties.length
            };
            callback(ajaxSrc);
          });
          return;
      }

      if (data.search.value == '' || data.search.value === currentAccount) {
        ids = createIdsList(data['start'], data['length'], bountyCount);
      } else {
        ajaxSrc = {
          'data': bounties,
          'draw': data.draw,
          'recordsTotal': bounties.length,
          'recordsFiltered': bounties.length
        };
        return callback(
          ajaxSrc
        );
      }

      bountyContract.methods.userBounties(currentAccount, ids).call()                            
        .then(events => {
          let bounties = events[0].map(bounty => {
            const json = JSON.parse(bounty.json)
            return {
              id: bounty.id,
              name: json.name,
              budget: web3.utils.fromWei(bounty.budget, 'ether'),
              startDate: bounty.startDate,
              keywords: `${json.kwds[0]}, ${json.kwds[1]}, ${json.kwds[2]}`,
              hunters: bounty.hunters.length,
            };
          });

          ajaxSrc = {
            'data': bounties,
            'draw': data.draw,
            'recordsTotal': recordsTotal,
            'recordsFiltered': recordsTotal
          };
          callback(
            ajaxSrc
          );
        })

    },
    'columns': [
      { "data": null,"sortable": false,
      render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
        }
      },
      {'data': 'id'},
      {'data': 'name',
      "render": function (data) {
        return `<button type="button" class="btn btn-link"><span>${data}</span></button>`
      }},
      {'data': 'budget',
       "render": function (data) {
          return `${Number(data).toFixed(2)}<small class='ml-2'>ETH</small>`
      }},
      {'data': 'startDate'},
      {'data': 'keywords'},
      {'data': 'hunters'},
    ],

  });
  $('#myBountiesTable tbody').on('click', 'button.btn', function () {
    let data = myBountiesTable.row( $(this).parents('tr') ).data();
    window.location = `/viewBounty?id=${data.id}`;
  });
};

var drawMyHuntersTable = function () {
  if (myHuntersTable) {
    myHuntersTable.destroy();
  }
  myHuntersTable = $('#myHuntersTable').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    'ajax': function (data, callback, settings) {
      let ids = [];
      let ajaxSrc = null;
      let recordsTotal = hunterCount - 1;
      ids = createIdsList(data['start'], data['length'], hunterCount);

      bountyContract.methods.userHunters(currentAccount, ids).call()
        .then(events => {
          let hunters = events;
          let bountiesId = events.map(hunter => hunter.bountyId);
          bountyContract.methods.getBounties(bountiesId).call()
          .then(events => {
            let bounties = events[0];
            let bountyHunters = mergeBountyHunters(bounties, hunters);
            ajaxSrc = {
              'data': bountyHunters,
              'draw': data.draw,
              'recordsTotal': recordsTotal,
              'recordsFiltered': recordsTotal
            };
            callback(
              ajaxSrc
            );
          })

        })

    },
    'columns': [
      { "data": null,"sortable": false,
      render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
        }
      },
      {'data': 'name',
      "render": function (data) {
        return `<button type="button" class="btn btn-link"><span>${data}</span></button>`
      }},
      {'data': 'keyword1'},
      {'data': 'keyword2'},
      {'data': 'keyword3'},
      {'data': 'budget'},
      {'data': 'url'},
    ],

  });
  $('#myHuntersTable tbody').on('click', 'button.btn', function () {
    let data = myHuntersTable.row( $(this).parents('tr') ).data();
    window.location = `/viewBounty?id=${data.bountyId}`;
  });
};

var getUsersCount = function () {
  bountyContract.methods.users(currentAccount).call().then(res => {
    bountyCount = Number(res.bountyCount);
    hunterCount = Number(res.huntersCount);
    $('#bountyCount').text(bountyCount);
    drawMyBountiesTable();
    drawMyHuntersTable();
  });
};

$(function () {
  checkMetamask.then(res => {
    $('#accountAddressMyBounties').text(truncateAddress(currentAccount));
    getUsersCount();
    onChangeAccount();
  })
  .catch(err => {
    console.log(err)
  })
});