var bountyId = null;
var myHuntersInfoTabIdle = null;
var huntersCount = null;

var onChangeAccount = function (startDate, duration, sender) {
  ethereum.on('accountsChanged', (accounts) => {
    if (accounts.length === 0) {
      window.location = window.location.origin;
    } else {
      currentAccount = accounts[0];
      $('#accountAddress').text(truncateAddress(currentAccount));
      timeIsExpired(startDate, duration, sender);
    }
  });
};

var getDuration = function (epoch) {
  epoch = Number(epoch)
  let oneMonthEpoch = 2678400;
  let leapYear = 31622400;
  let duration = '';
  if (epoch <= oneMonthEpoch) {
    return '1 Month';
  }
  if (epoch > 2*oneMonthEpoch && epoch <= 3*oneMonthEpoch) {
    return '3 Month';
  }
  if (epoch > 5*oneMonthEpoch && epoch <= 6*oneMonthEpoch) {
    return '6 Month';
  }
  if (epoch > 8*oneMonthEpoch && epoch <= 9*oneMonthEpoch) {
    return '9 Month';
  }
  if (epoch > 11*oneMonthEpoch && epoch <= leapYear) {
    return '12 Month';
  }
};

var getBountyInfo = function () {
  bountyContract.methods.viewInfo(bountyId).call({
    from: currentAccount
  })
  .then(res => {
    onChangeAccount(res.startDate, res.duration, res.sender);
    timeIsExpired(res.startDate, res.duration, res.sender);
    drawMyHuntersInfoTabIdle(res);
    const json = JSON.parse(res.json);
    addConditionsToPage(res.conds)
    $('#name').text(json.name);
    $('#budget').text(Number(web3.utils.fromWei(res.budget, 'ether')).toFixed(2));
    $('#bountyId').text('#'+res.id);
    $('#keyword1').text(json.kwds[0]);
    $('#keyword2').text(json.kwds[1]);
    $('#keyword3').text(json.kwds[2]);
    $('#startDate').text(moment.unix(Number(res.startDate)).format("YYYY-MM-DD HH:mm"));
    $('#duration').text(getDuration(res.duration));
    if (res.isDone) {
      $('#isDone').prop("checked", true);
    } else {
      $('#isDone').prop("checked", false);
    }
    $('#description').text(json.desc);
    $('#tos').text(json.tos);
    $('#url').text(json.url);
    $('#sender').text(truncateAddress(res.sender));
  })
};

var applyHunter = function () {
  const url = $('#urlApplyHunterId').val();
  if (!url) {
    return toastr.error('Enter the URL!');
  }
  bountyContract.methods.addHunter(bountyId, url).send({
    from: currentAccount
  })
  .on('receipt', function(receipt){
    toastr.success('Your transaction to add a hunter is approved.');
    getBalance();
  })
  .on('error', function(error, receipt) {
    toastr.error(error.message || 'Some thing wrong with this transaction!')
  });
};

var drawMyHuntersInfoTabIdle = function (bounty) {
  if (myHuntersInfoTabIdle) {
    myHuntersInfoTabIdle.destroy();
  }
  bountyContract.methods.getBountyHunters(bounty.id).call()
  .then(hunters => {
    hunters = hunters.map(hunter => {
      return {
        id: hunter.id,
        user_id: hunter.userId,
        url: hunter.url,
      }
    })
    myHuntersInfoTabIdle = $('#myHuntersInfoTabIdle').DataTable({
      "searching": false,
      "data": hunters,
      'columns': [
        { "data": null,"sortable": false,
        render: function (data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
          }
        },
        {'data': 'id'},
        {'data': 'user_id'},
        {'data': 'url'},
      ],
    });
  });

  $('#myHuntersInfoTabIdle tbody').on('click', 'button.btn', function () {
    let data = myHuntersInfoTabIdle.row( $(this).parents('tr') ).data();
    window.location = `/viewBounty?id=${data.bountyId}`;
  });
};

var timeIsExpired = function (startDate, duration, sender) {
  let expirationEpochDate = Number(startDate) + Number(duration);
  let epochNow = moment().unix();
  if (web3.utils.toChecksumAddress(currentAccount) == web3.utils.toChecksumAddress(sender)) {
    if (epochNow > expirationEpochDate) {
      $('#reBtnsId').removeClass('d-none').addClass('d-block');
    }
  } else {
    $('#reBtnsId').removeClass('d-block').addClass('d-none');
  }
};

var addConditionsToPage = function (conditions) {
  let conditionsCopy = JSON.parse(JSON.stringify(conditions));
  let rewards = new Array(Math.ceil(conditionsCopy.length / 4))
  .fill()
  .map(_ => conditionsCopy.splice(0, 4));
  rewards.forEach(item => {
    let rewardTmp = `<li class="list-group-item d-flex justify-content-between align-items-center">
                      ${item[0]} hunters ranks ${item[1]}-${item[2]} <span class="badge badge-success badge-pill p-2">${web3.utils.fromWei(item[3], 'ether')}
                      <small>ETH</small></span>
                    </li>`;
    $('#condsListId').append(rewardTmp);
  })

}

$(function () {
  bountyId = getParam('id');
  checkMetamask.then(res => {
    getBountyInfo();
  })
  .catch(err => {
    console.log(err)
  })
});
