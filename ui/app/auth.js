var account = null;

$(function () {
  $('#Authorization').on('click', function () {
    if (typeof window.ethereum !== 'undefined') {
      ethereum.request({ method: 'eth_requestAccounts' })
      .then(accounts => {
        window.location = '/explorer';
      })
      .catch(error => {
        console.log('error', error)
      });
    } else {
      return toastr.error('Please install metamask')
    }
  })
});