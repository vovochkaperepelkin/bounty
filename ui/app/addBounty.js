var $list = $("#conditionList .list");
var addedConditionTemplate = "<li class='list-inline-item my-1'><p class='btn btn-info pt-3'>{{fromRank}}<span class='badge badge-danger ml-2 remove'>×</span></p></li>";
var conditionList = [];
$list.on( "updatedList", updateList );

var onChangeAccount = function () {
  ethereum.on('accountsChanged', (accounts) => {
    if (accounts.length === 0) {
      window.location = window.location.origin;
    } else {
      currentAccount = accounts[0];
      $('#accountAddress').text(truncateAddress(currentAccount));
      getBalance();
    }
  });
};

var addBounty = function () {
  const name = $('#nameId').val();
  const budget = calculateBudget();
  const startDate = $('#startDateValueId').val();
  const durationTime = $('#durationId').val();
  const keyword1 = $('#keyword1Id').val();
  const keyword2 = $('#keyword2Id').val();
  const keyword3 = $('#keyword3Id').val();
  const description = $('#descriptionId').val();
  const tos = $('#tosId').val();
  const url = $('#urlId').val();
  let epochStartDate = moment(startDate, 'MM/DD/YYYY hh:mm').unix();
  let epochMonthsLater = moment(startDate, 'MM/DD/YYYY hh:mm').add(Number(durationTime), 'M').unix();
  let duration = epochMonthsLater - epochStartDate;

  if (!name || !startDate || !durationTime || !keyword1 ||
    !keyword2 || !keyword3 || !description || !tos || !url) {
      return toastr.error('You should complete the form!');
    }
    if (Number(budget) > Number(balance)) {
      return toastr.error('Insufficient ETH for the new bounty');
    }
    const conds = createConditions();
    if (conds.length == 0) {
      return toastr.error('Add some conditions!');
    }
    const json = {
    name: name,
    desc: description,
    kwds: [keyword1, keyword2, keyword3],
    tos: tos,
    url: url,
    v: appVersion
  }
  removeAllConditions();
  bountyContract.methods.addBounty(JSON.stringify(json), epochStartDate, duration, conds).send({
    value: web3.utils.toWei(String(budget), 'ether'),
    from: currentAccount
  })
  .on('transactionHash', function(hash){
    $('#transactionLinkId').show();
    $("#transactionHash").attr('href', ropstenEtherScanUrl + hash);
  })
  .on('receipt', function(receipt){
    toastr.success('Your transaction to add a new bounty is approved.');
    getBalance();
  })
  .on('error', function(error, receipt) {
    toastr.error(error.message || 'Some thing wrong with this transaction!')
  });
};

function updateList ( event, conditions ) {
  $list.html($.map( conditions, function ( condition ) {
      let text = `${condition.numberOfUsers} hunters ranks ${condition.fromRank}-${condition.toRank} (${condition.reward} ETH)<span class='d-none'>${condition.id}</span>`;
      return addedConditionTemplate.replace( /{{fromRank}}/, text);
  }).join(""));
}

var addCondition = function () {
  let numberOfUsers = $('#numberOfUsersId').val();
  let reward = $('#rewardId').val();
  let fromRank = $('#fromRankId').val();
  let toRank = $('#toRankId').val();
  if (!numberOfUsers || !reward || !fromRank || !toRank) {
    return toastr.error('Please fill the form!');
  }
  let conditionInfo = {
    numberOfUsers: numberOfUsers,
    fromRank: fromRank,
    toRank: toRank,
    reward: reward,
    id: generateId()
  };
  if (!conditionValidation(conditionInfo)) {
    return toastr.error('Invalid input!');
  }
  conditionList.push(conditionInfo);
  $list.trigger( "updatedList", [ conditionList ] );
  clearConditionForm();
  $('#conditionModal').modal('hide');
};

var generateId = function () {
  return Math.floor(Math.random() * 1000000000);
};

var removeCondition = function (element) {
  const id = element.parent().find('span')[0].textContent.trim();
  conditionList = conditionList.filter(item => item.id !== Number(id));
  $list.trigger( "updatedList", [ conditionList ] );
  element.parent().remove();
};

var calculateBudget = function () {
  let budget = conditionList.reduce((acc, val) => {
    return acc + (Number(val.reward) * Number(val.numberOfUsers));
  }, 0)
  return budget;
};

var createConditions = function () {
  conditionList.forEach(condition => delete condition.id);
  let conditionListClone = JSON.parse(JSON.stringify(conditionList));
  let conditionListValues = conditionListClone.map(item => {
    item.reward = web3.utils.toWei(String(item.reward), 'ether')
    return Object.values(item)
  });
  let conditionsAggregate = [];
  conditionListValues.forEach(item => {
    item.forEach(value => {
      conditionsAggregate.push(String(value))
    })
  })
  return conditionsAggregate;
}

var removeAllConditions = function () {
  conditionList = []
  $('#conditionList ul').empty();
};

var conditionValidation = function (conditionInfo) {
  const usersBetweenRanks = (conditionInfo.toRank - conditionInfo.fromRank) + 1;
  if (Number(conditionInfo.numberOfUsers) > usersBetweenRanks ||
      Number(conditionInfo.fromRank) >= Number(conditionInfo.toRank)) {
    return false;
  }
  for (let i = 0; i < conditionList.length; i++) {
    if (conditionList[i].fromRank == conditionInfo.fromRank ||
        conditionList[i].toRank == conditionInfo.toRank) {
          return false;
    }
  }
  return true;
};

var openConditionModal = function () {
  $('#conditionModal').modal('show');
};

var clearConditionForm = function () {
  $('#numberOfUsersId').val(null);
  $('#rewardId').val(null);
  $('#fromRankId').val(null);
  $('#toRankId').val(null);
};

$(function () {
  $('#startDateId').datetimepicker();

  checkMetamask.then(res => {
    $('#accountAddress').text(truncateAddress(currentAccount));
    getBalance();
    onChangeAccount();
  })
  .catch(err => {
    console.log(err)
  })

  $('#addBountyBtn').on('click', function() {
    addBounty();
  });
  $(document).on('click', 'span.remove ', function () {
    removeCondition($(this));
  });
})