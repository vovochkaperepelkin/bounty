var lastBountyId = null;

var onChangeAccount = function () {
  ethereum.on('accountsChanged', (accounts) => {
    if (accounts.length === 0) {
      window.location = window.location.origin;
    } else {
      currentAccount = accounts[0];
    }
  });
};

var drawExplorerTable = function () {
  let explorerTable = $('#explorerTable').DataTable({
    "processing": true,
    "serverSide": true,
    'ajax': function (data, callback, settings) {
      let bounties = [];
      let sender = '';
      let ids = [];
      let recordsTotal = Number(lastBountyId) - 1;
      if (!web3.utils.isAddress(data.search.value) &&
          data.search.value !== '' &&
          !isNumeric(data.search.value)) {
        return callback({
          'data': bounties,
          'draw': data.draw,
          'recordsTotal': 0,
          'recordsFiltered': 0
        })
      } else {
        if (isNumeric(data.search.value)) {
          ids.push(data.search.value);
        } else {
          sender = data.search.value;
        }
      }
      if (data.search.value == '') {
        ids = createIdsList(data['start'], data['length']);
      }
      bountyContract.getPastEvents("NewBounty",{
        fromBlock: 8838945,
        toBlock: 'latest',
        filter: {
          sender: sender,
          id: ids
        }    
      })                              
    .then(events => {
      let bountiesId = events.map(bounty => bounty.returnValues.id);
      bountiesId.reverse();

      bountyContract.methods.getBounties(bountiesId).call()
      .then(events => {
        let bounties = events[0].map(bounty => {
          const json = JSON.parse(bounty.json)
          return {
            id: bounty.id,
            name: json.name,
            budget: web3.utils.fromWei(bounty.budget, 'ether'),
            startDate: moment.unix(Number(bounty.startDate)).format("YYYY-MM-DD HH:mm:ss"),
            keywords: `${json.kwds[0]}, ${json.kwds[1]}, ${json.kwds[2]}`,
            hunters: bounty.hunters.length,
          };
        });

        let ajaxSrc = {
          'data': bounties,
          'draw': data.draw,
          'recordsTotal': recordsTotal,
          'recordsFiltered': recordsTotal
        };

        if (sender !== '') {
          ajaxSrc['recordsFiltered'] = data['length'];
          ajaxSrc['recordsTotal'] = data['length'];
        } else if (bounties.length == 0 || bounties.length == 1) {
          ajaxSrc['recordsFiltered'] = bounties.length;
          ajaxSrc['recordsTotal'] = bounties.length;
        }
        callback(
          ajaxSrc
        );
      })
    })

    },
    'columns': [
      {'data': 'id'},
      {'data': 'name',
      "render": function (data) {
        return `<button type="button" class="btn btn-link"><span>${data}</span></button>`
      }},
      {'data': 'budget',
      "render": function (data) {
        return `${Number(data).toFixed(2)}<small class='ml-2'>ETH</small>`
      }},
      {'data': 'startDate'},
      {'data': 'keywords'},
      {'data': 'hunters'},
    ],

  });
  $('#explorerTable tbody').on('click', 'button.btn', function () {
    let data = explorerTable.row( $(this).parents('tr') ).data();
    window.location = `/viewBounty?id=${data.id}`;
  });
};

var getLastBountyId = function () {
  bountyContract.methods.lastBountyId().call().then(res => {
    lastBountyId = res;
    drawExplorerTable();
  });
};

var createIdsList = function (start, pageLength) {
  let ids = [];
  let pageNumber = (start / pageLength) + 1;
  let end = (lastBountyId - 1) - ((pageNumber - 1) * pageLength);
  for(let i=0; i < pageLength; i++){
    let id = (end - i);
    if (id == 0) {
      break;
    }
    ids.push(id);
  }
  return ids;
};

$(function () {
  checkMetamask.then(res => {
    getLastBountyId();
    onChangeAccount();
  })
  .catch(err => {
    console.log(err)
  })
})