## Install  
  
1- Install requirements  
NodeJS 10+  
bower  
MongoDB  
  
2- Install UI components  
$ cd ui  
$ bower install  
   
3- Run backend server  
$ npm install  
$ nodejs server.js  
The website will run on port 3300  
   
Smart contracts are also in the repository. But they are already deployed.  
  
