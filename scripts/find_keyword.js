const axios = require('axios');

let promises = [];
let organicResults = [];

let rankingInquiry = async (db, bountyHunters) => {
  let params = {
    api_key: "B53C1E166B784DA2BA513EE094692CDA",
    num: 40
  }

  for (let bountyId of Object.keys(bountyHunters)) {
    let hunters = bountyHunters[bountyId].hunters;
    let keywords = bountyHunters[bountyId].keywords;
    params.q = keywords.join();

    const response = await axios.get('https://api.scaleserp.com/search', { params });
    organicResults.push(response.data.organic_results)

    organicResults.map(item => {
      hunters.map(async element => {
        if (item.link == element.url) {
          element.position = item.position;
          element.snippet_matched = item.snippet_matched;
          element.title = item.title;
          let res = await db.collection('hunters').updateOne({_id: element._id}, { $set: element });
          promises.push(res)
        }
      })
    })
  }
  const final = await Promise.all(promises);
  console.log('Process completed')
  db.close();
};

module.exports = rankingInquiry;