var contractAbi = [
  {
    "inputs":[
      
    ],
    "stateMutability":"nonpayable",
    "type":"constructor"
  },
  {
    "anonymous":false,
    "inputs":[
      {
        "indexed":true,
        "internalType":"address",
        "name":"sender",
        "type":"address"
      },
      {
        "indexed":true,
        "internalType":"uint256",
        "name":"id",
        "type":"uint256"
      },
      {
        "indexed":false,
        "internalType":"uint256",
        "name":"budget",
        "type":"uint256"
      },
      {
        "indexed":false,
        "internalType":"uint256",
        "name":"startDate",
        "type":"uint256"
      },
      {
        "indexed":false,
        "internalType":"uint256",
        "name":"duration",
        "type":"uint256"
      },
      {
        "indexed":false,
        "internalType":"string",
        "name":"json",
        "type":"string"
      }
    ],
    "name":"NewBounty",
    "type":"event"
  },
  {
    "inputs":[
      {
        "internalType":"string",
        "name":"_json",
        "type":"string"
      },
      {
        "internalType":"uint256",
        "name":"_startDate",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"_duration",
        "type":"uint256"
      },
      {
        "internalType":"uint256[]",
        "name":"_conds",
        "type":"uint256[]"
      }
    ],
    "name":"addBounty",
    "outputs":[
      
    ],
    "stateMutability":"payable",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256",
        "name":"_bountyId",
        "type":"uint256"
      },
      {
        "internalType":"string",
        "name":"_url",
        "type":"string"
      }
    ],
    "name":"addHunter",
    "outputs":[
      
    ],
    "stateMutability":"nonpayable",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256",
        "name":"",
        "type":"uint256"
      }
    ],
    "name":"bounties",
    "outputs":[
      {
        "internalType":"address",
        "name":"sender",
        "type":"address"
      },
      {
        "internalType":"uint256",
        "name":"id",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"budget",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"startDate",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"duration",
        "type":"uint256"
      },
      {
        "internalType":"bool",
        "name":"isDone",
        "type":"bool"
      },
      {
        "internalType":"uint256",
        "name":"totalHunters",
        "type":"uint256"
      },
      {
        "internalType":"string",
        "name":"json",
        "type":"string"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256",
        "name":"",
        "type":"uint256"
      }
    ],
    "name":"bountyHunters",
    "outputs":[
      {
        "internalType":"uint256",
        "name":"id",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"bountyId",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"userId",
        "type":"uint256"
      },
      {
        "internalType":"string",
        "name":"url",
        "type":"string"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256",
        "name":"",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"",
        "type":"uint256"
      }
    ],
    "name":"bountyUsers",
    "outputs":[
      {
        "internalType":"bool",
        "name":"",
        "type":"bool"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256[]",
        "name":"indexes",
        "type":"uint256[]"
      }
    ],
    "name":"getBounties",
    "outputs":[
      {
        "components":[
          {
            "internalType":"address",
            "name":"sender",
            "type":"address"
          },
          {
            "internalType":"uint256",
            "name":"id",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"budget",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"startDate",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"duration",
            "type":"uint256"
          },
          {
            "internalType":"bool",
            "name":"isDone",
            "type":"bool"
          },
          {
            "internalType":"uint256",
            "name":"totalHunters",
            "type":"uint256"
          },
          {
            "internalType":"uint256[]",
            "name":"hunters",
            "type":"uint256[]"
          },
          {
            "internalType":"string",
            "name":"json",
            "type":"string"
          },
          {
            "internalType":"uint256[]",
            "name":"conds",
            "type":"uint256[]"
          }
        ],
        "internalType":"struct SLAP10.Bounty[]",
        "name":"",
        "type":"tuple[]"
      },
      {
        "internalType":"uint256[][]",
        "name":"",
        "type":"uint256[][]"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256",
        "name":"_bountyId",
        "type":"uint256"
      }
    ],
    "name":"getBountyHunters",
    "outputs":[
      {
        "components":[
          {
            "internalType":"uint256",
            "name":"id",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"bountyId",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"userId",
            "type":"uint256"
          },
          {
            "internalType":"string",
            "name":"url",
            "type":"string"
          }
        ],
        "internalType":"struct SLAP10.BountyHunter[]",
        "name":"",
        "type":"tuple[]"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256[]",
        "name":"indexes",
        "type":"uint256[]"
      }
    ],
    "name":"getHunters",
    "outputs":[
      {
        "components":[
          {
            "internalType":"uint256",
            "name":"id",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"bountyId",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"userId",
            "type":"uint256"
          },
          {
            "internalType":"string",
            "name":"url",
            "type":"string"
          }
        ],
        "internalType":"struct SLAP10.BountyHunter[]",
        "name":"",
        "type":"tuple[]"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256",
        "name":"",
        "type":"uint256"
      }
    ],
    "name":"idToAddress",
    "outputs":[
      {
        "internalType":"address",
        "name":"",
        "type":"address"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      
    ],
    "name":"lastBountyHunterId",
    "outputs":[
      {
        "internalType":"uint256",
        "name":"",
        "type":"uint256"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      
    ],
    "name":"lastBountyId",
    "outputs":[
      {
        "internalType":"uint256",
        "name":"",
        "type":"uint256"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      
    ],
    "name":"lastUserId",
    "outputs":[
      {
        "internalType":"uint256",
        "name":"",
        "type":"uint256"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"address",
        "name":"_admin",
        "type":"address"
      }
    ],
    "name":"updateAdmin",
    "outputs":[
      {
        "internalType":"bool",
        "name":"",
        "type":"bool"
      }
    ],
    "stateMutability":"nonpayable",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"address",
        "name":"user",
        "type":"address"
      },
      {
        "internalType":"uint256[]",
        "name":"indexes",
        "type":"uint256[]"
      }
    ],
    "name":"userBounties",
    "outputs":[
      {
        "components":[
          {
            "internalType":"address",
            "name":"sender",
            "type":"address"
          },
          {
            "internalType":"uint256",
            "name":"id",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"budget",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"startDate",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"duration",
            "type":"uint256"
          },
          {
            "internalType":"bool",
            "name":"isDone",
            "type":"bool"
          },
          {
            "internalType":"uint256",
            "name":"totalHunters",
            "type":"uint256"
          },
          {
            "internalType":"uint256[]",
            "name":"hunters",
            "type":"uint256[]"
          },
          {
            "internalType":"string",
            "name":"json",
            "type":"string"
          },
          {
            "internalType":"uint256[]",
            "name":"conds",
            "type":"uint256[]"
          }
        ],
        "internalType":"struct SLAP10.Bounty[]",
        "name":"",
        "type":"tuple[]"
      },
      {
        "internalType":"uint256[][]",
        "name":"",
        "type":"uint256[][]"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"address",
        "name":"user",
        "type":"address"
      },
      {
        "internalType":"uint256[]",
        "name":"indexes",
        "type":"uint256[]"
      }
    ],
    "name":"userHunters",
    "outputs":[
      {
        "components":[
          {
            "internalType":"uint256",
            "name":"id",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"bountyId",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"userId",
            "type":"uint256"
          },
          {
            "internalType":"string",
            "name":"url",
            "type":"string"
          }
        ],
        "internalType":"struct SLAP10.BountyHunter[]",
        "name":"",
        "type":"tuple[]"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"address",
        "name":"",
        "type":"address"
      }
    ],
    "name":"users",
    "outputs":[
      {
        "internalType":"uint256",
        "name":"id",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"bountyCount",
        "type":"uint256"
      },
      {
        "internalType":"uint256",
        "name":"huntersCount",
        "type":"uint256"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  },
  {
    "inputs":[
      {
        "internalType":"uint256",
        "name":"index",
        "type":"uint256"
      }
    ],
    "name":"viewInfo",
    "outputs":[
      {
        "components":[
          {
            "internalType":"address",
            "name":"sender",
            "type":"address"
          },
          {
            "internalType":"uint256",
            "name":"id",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"budget",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"startDate",
            "type":"uint256"
          },
          {
            "internalType":"uint256",
            "name":"duration",
            "type":"uint256"
          },
          {
            "internalType":"bool",
            "name":"isDone",
            "type":"bool"
          },
          {
            "internalType":"uint256",
            "name":"totalHunters",
            "type":"uint256"
          },
          {
            "internalType":"uint256[]",
            "name":"hunters",
            "type":"uint256[]"
          },
          {
            "internalType":"string",
            "name":"json",
            "type":"string"
          },
          {
            "internalType":"uint256[]",
            "name":"conds",
            "type":"uint256[]"
          }
        ],
        "internalType":"struct SLAP10.Bounty",
        "name":"bounty",
        "type":"tuple"
      }
    ],
    "stateMutability":"view",
    "type":"function"
  }
];

module.exports = contractAbi