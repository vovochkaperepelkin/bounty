var MongoClient = require('mongodb').MongoClient;
const mongoUrl = process.env.MONGO_URI;

function connectDb(){
    return new Promise(function (resolve, reject) {
        MongoClient.connect(mongoUrl, function(err, db) {
            if(err)
                return reject(err);
            //console.log(db);
            resolve(db);
        });
    })
}

module.exports = connectDb;
