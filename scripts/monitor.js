var Contract = require('web3-eth-contract');
require('dotenv').config();
const abi = require('./abi');
const connectDB = require('./db');
const rankingInquiry = require('./find_keyword')

Contract.setProvider('https://ropsten.infura.io/v3/2a14c8fe620544eaa19aadc73238dde2');
const contractAddress = process.env.CONTRACT_ADDRESS;
var contract = new Contract(abi, contractAddress);

var db = null;

let getKeywords = async () => {
  let bounties = await db.collection('bounties').find({}).toArray();
    let bountyHunters = {};
      for (let bounty of bounties) {
      let hunters = await db.collection('hunters').find({bountyId: bounty.id}).toArray()
          if (hunters.length !== 0) {
          bountyHunters[bounty.id] = {
              keywords: JSON.parse(bounty.json).kwds,
              hunters: hunters
          }
          }
          }
      rankingInquiry(db, bountyHunters)
};

let getBounties = (ids) => {
  contract.methods.getBounties(ids).call()
  .then(res => {
    res = res[0].map(bounty => {
      return {
        id: bounty.id,
        budget: bounty.budget,
        conds: bounty.conds,
        duration: bounty.duration,
        hunters: bounty.hunters,
        isDone: bounty.isDone,
        json: bounty.json,
        sender: bounty.sender,
        startDate: bounty.startDate,
        totalHunters: bounty.totalHunters,
      }
    })
    db.collection('bounties').insertMany(res)
    .then(x => {
      console.log('Bounties up to date')
      console.log('Waiting ...')
      getLastHunterId();
    })
    .catch(err => {
      console.log(err)
    })
  }).catch(error => {
    console.log(error)
  })
};

let getHunters = (ids) => {
  contract.methods.getHunters(ids).call()
  .then(res => {
    // console.log(res[0])
    res = res.map(hunter => {
      return {
        id: hunter.id,
        bountyId: hunter.bountyId,
        url: hunter.url,
        userId: hunter.userId,
      }
    })
    db.collection('hunters').insertMany(res)
    .then(x => {
      console.log('Hunters up to date')
      console.log('Waiting ...')
      getKeywords();
    })
    .catch(err => {
      console.log(err)
    })
  }).catch(error => {
    console.log(error)
  })
};


let getLastBountyId = () => {
  contract.methods.lastBountyId().call().then(res => {
    lastBountyId = Number(res);
    db.collection('bounties').findOne({}, {sort:{$natural:-1}})
      .then(bounty => {
        let idOfLastSavedBounty = 1;
        if (bounty) {
          idOfLastSavedBounty = Number(bounty.id);
        }

        const ids = numberRange(idOfLastSavedBounty, lastBountyId);
        if (ids.length == 0) {
          console.log('Bounties already up to date.');
          console.log('Waiting ...');
          getLastHunterId();
          return;
        }
        getBounties(ids);
      })
      .catch(err => {
        console.log(err)
      })
  });
};

let getLastHunterId = () => {
  contract.methods.lastBountyHunterId().call().then(res => {
    lastHunterId = Number(res);
    db.collection('hunters').findOne({}, {sort:{$natural:-1}})
      .then(hunter => {
        let idOfLastSavedHunter = 1;
        if (hunter) {
          idOfLastSavedHunter = Number(hunter.id);
        }
        const ids = numberRange(idOfLastSavedHunter, lastHunterId);
        if (ids.length == 0) {
          console.log('Hunters already up to date.');
          console.log('Waiting ...');
          getKeywords()
          return;
        }
        getHunters(ids);
      })
      .catch(err => {
        console.log(err)
      })
  });
};


let numberRange = (start, end) => {
  if (start == end - 1) {
    return []
  }
  return new Array(end - start).fill().map((d, i) => i + start);
}

connectDB()
.then(_db => {
  db = _db;
  return db
})
.then(() => {
    getLastBountyId();
  }).catch(error => {
    console.error(error);
  });
