const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;
const host = process.env.HOST || "0.0.0.0";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', path.join(__dirname, 'ui'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);


if (true || process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));

  // Handle React routing, return all requests to React app
  ['css', "bower_components/bootstrap/dist/js", 'bower_components/toastr', "bower_components/moment/min",
  'vendors/@coreui/coreui/js', 'vendors/@coreui/icons/svg', 'vendors/@coreui/icons/js', 'bower_components/jquery/dist',
  'assets/brand', 'app', 'assets/favicon', 'js'].map(x => {
    app.get(`/${x}/:filePath`, function (req, res) {      
      res.sendFile(path.join(__dirname, 'ui/'+x, req.params.filePath));
    });
  });

  app.get(['/addBounty.html', '/addBounty'], function (req, res) {
    res.render('views/pages/addBounty.html', 
    )
  });

  app.get(["/explorer.html", '/explorer'], function(req, res){
    res.render('views/pages/explorer.html')
  });

  app.get(["/myBounty.html", '/myBounty'], function(req, res){
    res.render('views/pages/myBounty.html')
  });

  app.get(["/myBounties.html", '/myBounties'], function(req, res){
    res.render('views/pages/myBounties.html')
  });

  app.get(["/index.html", '/authorization', '/'], function(req, res){
    res.render('views/pages/index.html')
  });

  app.get(["/viewBounty.html", '/viewBounty'], function(req, res){
    res.render('views/pages/viewBounty.html')
  });

}

app.listen(port, host, () => console.log(`Listening on port ${port}`));
